#!/bin/bash


function list(){
	unzip -l "$1"
}
function preview(){
	unzip -p "$1" "$2"
}
function add_arq(){
	zip -j -u "$1" "$2"
}
function rm_arq(){
	zip -d "$1" "$2"
}
function extract(){
	unzip "$1"
}
function extract_esp(){
	unzip "$1" "$2"
}


echo "Ferramenta para utilizacao dos recusros zip/unzip."
echo 
read -p "Escolha 1 se ja tiver algum arquivo .zip e 2 caso queira criar um:" op
echo

case $op in

	1) 
		read -p "Escolha o arquivo .zip que deseja manipular: " arq_zip 
		if  [ "${arq_zip##*.}" != "zip" ] || [ ! -e "$arq_zip" ]; then
			echo
			echo "O arquivo passado é inexistente ou nao tem o formato '.zip'"
			exit 1
		fi
		echo 
		;;
		
	2) 
		inpts=()
		while true; do
       			read -p "Digite o nome do arquivo que deseja compactar, e 'q' caso tenha acabado: " arq
			echo
			
			if [ "$arq" == "q" ]; then
			       break
		       	else
				if [ ! -e "$arq" ]; then
			 		echo "arquivo ou caminho inexistente."
					echo
				else
					inpts+=("$arq")
				fi
			fi			
		done 

		echo
		read -p "Nome do arquivo de destino: " arq_zip
		echo

		arq_zip="$arq_zip.zip"

		zip -j "$arq_zip" "${inpts[0]}" > /dev/null
		
		for i in "${inpts[@]:1}"; do
			zip -j -u "$arq_zip" "$i" > /dev/null
		done

		echo
		echo "Arquivo $arq_zip criado com sucesso."
		echo
		;;

	*) 	
		echo "Opcao invalida." 
		exit 1
		;;

esac

while true; do
	
	echo
	echo
	echo "1) Listar o conteudo do arquivo .zip."
	echo "2) Pre-visualizar algum arquivo que esta compactado neste .zip."
	echo "3) Adicionar arquivo ao .zip."
	echo "4) Remover arquivo ao .zip."
	echo "5) Extrair todo o conteudo do .zip."
	echo "6) Extrair arquivo especifico do .zip."
	echo 
	echo "0) Sair do programa."
	echo
	read -p "Escolha um recurso para utilizar dentre a lista:" op
	echo
	echo

	case $op in
	
		1) 
			list $arq_zip ;;
		2) 
			read -p "Digite o arquivo que deseja visualizar dentro do zip: " arq
			echo "conteudo do arquivo:"
			preview $arq_zip $arq
			;;
		3) 	read -p "Digite o arquivo que deseja adicionar ao zip: " arq 
			add_arq $arq_zip $arq
			;;
		4) 	read -p "Digite o arquivo que deseja remover do zip: " arq 
			rm_arq $arq_zip $arq	
			;;
		5) 	extract $arq_zip
			;;
		6) 	read -p "Digite o arquivo que deseja extrair do zip: " arq 
			extract_esp $arq_zip $arq
			;;
		0) exit 1;;
		*) echo "opcao invalida." ;;
	esac
done


