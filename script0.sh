#!/bin/bash	


echo -e $BASH_VERSION "\$BASH_VERSION \tArmazena a versão do shell bash em execução, em formato legível por humanos."
echo -e $CDPATH "\$CDPATH \tCaminho de busca do comando cd"
echo -e $DIRSTACK"\$DIRSTACK \tPilha de diretórios disponíveis para os comandos pushd e popd"
echo -e $DISPLAY "\$DISPLAY \tConfigura o valor do display X"
echo -e $EDITOR "\$EDITOR \tConfigurar o editor de texto padrão do sistema"
echo -e $HISTFILE "\$HISTFILE \tNome do arquivo no qual o histórico de comandos é salvo"
echo -e $HISTFILESIZE "\$HISTFILESIZE \tNúmero máximo de linhas contidas no arquivo de histórico"
echo -e $HISTSIZE "\$HISTSIZE \tNúmero de comandos a serem armazenados no histórico de comandos. O valor padrão é 500." 
echo -e $HOME "\$HOME \tDiretório pessoal do usuário atual."
echo -e $HOSTNAME "\$HOSTNAME \tNome do computador." 
echo -e $IFS "\$IFS \t“Internal Field Separator” (Separador de Campos Interno). Caractere usado para separar palavras após a expansão e para dividir linhas em palavras ao usar o comando read do shell. O valor padrão é <espaço><tab><newline>"
echo -e $LANG "\$LANG \tConfigurações de localização e idioma atuais, incluindo a codificação de caracteres." 
echo -e $PATH "\$PATH \tCaminhos de busca para os comandos. É uma lista de diretórios, separados por dois-pontos (:), nos quais o shell procura pelos executáveis dos comandos."
echo -e $PS1 "\$PS1 \tConfigurações do prompt do usuário. "
echo -e $PWD "\$PWD \tValor do diretório de trabalho atual"
echo -e $RANDOM "\$RANDOM \tRetorna um número aleatório entre 0 e 32767." 
echo -e $TMOUT "\$TMOUT \tTimeout padrão do comando read do shell."
echo -e $TERM "\$TERM \tTipo de terminal de login do usuário. "
echo -e $USER "\$USER \tUsuário logado atualmente."















