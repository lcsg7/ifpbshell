#!/bin/bash

#Dupla: Cassio Bastos e Lucas Gabriel

# needs to download sshpass for using this tool

LOGIN=$(zenity \
	--title "Login" \
	--forms --text "Session" \
	--text "Transfer Modes: 1 - (Server-Client) | 2 - (Client-Server)" \
	--add-entry "Transfer Mode (1 or 2):" \
	--add-entry "Hostname(ip):" \
	--add-entry "Username:" \
	--add-password "Password: " \
	--add-entry "File to copy" \
	--add-entry "Path you want the file to go")

mode=$(echo $LOGIN | awk -F \| '{print $1}')
host=$(echo $LOGIN | awk -F \| '{print $2}')
user=$(echo $LOGIN | awk -F \| '{print $3}')
password=$(echo $LOGIN | awk -F \| '{print $4}')
file=$(echo $LOGIN | awk -F \| '{print $5}')
path=$(echo $LOGIN | awk -F \| '{print $6}')

function de_la_pa_ca(){
	sshpass -p $password scp -o StrictHostKeyChecking=no $user@$host:$file $path
}
function de_ca_pa_la(){
	sshpass -p $password scp -o StrictHostKeyChecking=no $file $user@$host:$path
}

if [ $mode = "1" ]; then de_la_pa_ca; fi

if [ $mode = "2" ]; then de_ca_pa_la; fi
