#!/bin/bash



echo "existem 3 meios de criar variáveis: 1-atribuindo o valor de um comando shell, 2- atribuindo um valor diretamente, 3- atribuindo atravez de uma entrada."

echo -e "1- ex: a=\$(ls -l)"
echo -e "2- ex: a='boa noite, amizade'"
echo -e "3- ex: read a / a=\$1"


echo "Diferença entre Entrada do Usuário e Parâmetros da Linha de Comando:
    Entrada do Usuário: Ao usar read, você está solicitando explicitamente que o usuário insira um valor durante a execução do script. O valor será inserido interativamente.

    Parâmetros da Linha de Comando: Parâmetros da linha de comando são valores fornecidos quando você inicia um script Bash. Eles são acessados usando as variáveis especiais $1, $2, etc., onde $1 é o primeiro parâmetro, $2 é o segundo, e assim por diante."


echo "Variáveis Automáticas:

Variáveis automáticas são variáveis predefinidas pelo Bash e que contêm informações úteis sobre o ambiente ou a execução do script. Alguns exemplos comuns de variáveis automáticas incluem:"

echo -e $0 "\$0: nome do script em execucao"
echo -e $$ "\$$: ID do processo do script atual."
