#!/bin/bash

t1=0
t2=0
t3=0


t1=$(($t1+$(find "$1" -type f  -name "*.xls" -type f | wc -l)))
t1=$(($t1+$(find "$2" -type f  -name "*.xls" -type f | wc -l)))
t1=$(($t1+$(find "$3" -type f  -name "*.xls" -type f | wc -l)))

t2=$(($t2+$(find "$1" -type f  -name "*.bmp" -type f | wc -l)))
t2=$(($t2+$(find "$2" -type f  -name "*.bmp" -type f | wc -l)))
t2=$(($t2+$(find "$3" -type f  -name "*.bmp" -type f | wc -l)))

t3=$(($t3+$(find "$1" -type f  -name "*.docx" -type f | wc -l)))
t3=$(($t3+$(find "$2" -type f  -name "*.docx" -type f | wc -l)))
t3=$(($t3+$(find "$3" -type f  -name "*.docx" -type f | wc -l)))



echo -e "aqrs .xls: $t1"
echo -e "arqs .bmp: $t2"
echo -e "arqs .docx: $t3"
