#!/bin/bash
while true; do
    echo "Menu de Customização:"
    echo "1. * Mudança de cor"
    echo "2. * Exibir apenas o nome do usuário"
    echo "3. * Prompt em duas linhas (a 1a com as informações, a 2a apenas com o $)"
    echo "4. * Customização especial"
    echo "5. * Restaurar configuração padrão"
    echo "6. * Sair"

    read -p "Escolha uma opção: " opcao

    case $opcao in
        1) 	echo "Escolha a cor que deseja utilizar"
		echo "Red = 41,Vermelho=31,Green=42,Verde=32,Yellow=43,Amarelo=33,Blue=44,Azul=34,Purple=45,Roxo=35,Cyan=46,Ciano=36,White=47,Branco=37"
		read op 
		case $op in
			[3][1-9]|[4][0-7]) source apply.sh 1 $op;;
			*) echo "opcao invalida";;
		esac ;;
		
        2) source apply.sh 2;;
        3) source apply.sh 3;;
	4) 	echo "digite uma palavra para adicinar no seu prompt"
		read entry 
		source apply.sh 4 $entry;;
	5) source apply.sh 5;;
	6) break;;
        *) echo "Opção inválida. Tente novamente.";;
    esac
done 

