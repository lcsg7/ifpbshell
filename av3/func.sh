#!/bin/bash



function change_color(){
	color="\[\033[0;$1m\]"
	reset="\033[0m"
	#actual_color=$(echo -n "$PS1" | sed -n 's/.*\[\e\[\([0-9;]*\)m.*/\1/p')
	att="${color}${PS1}${reset}"
	export PS1=$att
}
function just_username(){
	export PS1='[\u]$: '
}
function two_lines(){
	export PS1="[\u@\h]:\w$\n\033[0m\$ "
}

function extra (){
	PS1="\[\033[0;34m\](\u@\h\[\033[0m\])-[\[\033[0;36m\]\w]\[\033[0m\]\n$ "
	att="${PS1%??}${1}$ "
	PS1=$att
}

